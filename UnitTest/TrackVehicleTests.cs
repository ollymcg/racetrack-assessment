﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RaceTrack.Models;
using RaceTrack.Services;

namespace UnitTest
{
    [TestClass]
    public class TrackVehicleTests
    {
        [TestMethod]
        public void TestInspection()
        {
            TrackService _trackService = new TrackService();

            // Should pass
            Vehicle car = new Vehicle()
            {
                Make = "Toyota",
                Model = "Camry",
                Year = 2018,
                VehType = "Car",
                TowStrap = true,
                TirePercentage = 85.0M,
                ExtraClearance = 0,
            };

            // Should fail
            Vehicle truck = new Vehicle()
            {
                Make = "Toyota",
                Model = "4Runner",
                Year = 2018,
                VehType = "Truck",
                TowStrap = true,
                TirePercentage = 90.0M,
                ExtraClearance = 6,
            };

            Assert.IsTrue(_trackService.InspectVehicle(car));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => _trackService.InspectVehicle(truck));

        }
    }
}
