﻿using RaceTrack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaceTrack.Services
{
    public class TrackService
    {
        public void LogVehicles()
        {
            System.Diagnostics.Debug.WriteLine("New vehicles on the track");
            return;
        }

        public bool InspectVehicle(Vehicle vehicle)
        {
            bool passed = false;

            if (vehicle.TowStrap)
            {
                switch (vehicle.VehType)
                {
                    case "Car":
                        if (vehicle.TirePercentage >= 85.0M)
                        {
                            passed = true;
                        }
                        else
                        {
                            //throw new ArgumentOutOfRangeException("tire wear");
                            break;
                        }
                        break;
                    case "Truck":
                        if (vehicle.ExtraClearance <= 5)
                        {
                            passed = true;
                        }
                        else
                        {
                            //throw new ArgumentOutOfRangeException("clearance");
                            break;
                        }
                        break;
                    default:
                        break;
                }
            }
            string output = (passed) ? "Passed!" : "Failed!";

            System.Diagnostics.Debug.WriteLine(output);

            return passed;
        }
    }
}