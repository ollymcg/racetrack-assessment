﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaceTrack.Models;
using RaceTrack.Services;

namespace RaceTrack.Controllers
{
    public class VehicleController : Controller
    {
        private VehicleContext dbVehicle = new VehicleContext();
        private TrackContext dbTrack = new TrackContext();
        TrackService _trackService = new TrackService();

        // GET: Vehicle
        public ActionResult Index()
        {
            TrackVehicle inspectedVeh = dbTrack.TrackVehicles.OrderByDescending(v => v.ID).FirstOrDefault();
            ViewBag.Inspecting = (inspectedVeh != null) ? inspectedVeh.VehType : null;

            ViewBag.Vehicles = dbVehicle.Vehicles.ToList();
            ViewBag.Track = dbTrack.TrackVehicles.ToList();
            return View();
        }

        // GET: Vehicle/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vehicle/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Make,Model,Year,VehType,TowStrap,TirePercentage,ExtraClearance")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                dbVehicle.Vehicles.Add(vehicle);
                dbVehicle.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vehicle);
        }

        public ActionResult AddToTrack(int id)
        {
            int count = dbTrack.TrackVehicles.Count();

            // Don't add if there are already 5 vehicles
            if (count < 5)
            {
                Vehicle vehicle = dbVehicle.Vehicles.Where(v => v.ID == id).FirstOrDefault();

                bool passed = _trackService.InspectVehicle(vehicle);

                if (passed)
                {
                    TrackVehicle TrackVehicle = new TrackVehicle();
                    TrackVehicle.Make = vehicle.Make;
                    TrackVehicle.Model = vehicle.Model;
                    TrackVehicle.Year = vehicle.Year;
                    TrackVehicle.VehType = vehicle.VehType;

                    dbTrack.TrackVehicles.Add(TrackVehicle);
                    dbTrack.SaveChanges();
                    
                }
            }
            return RedirectToAction("Index");
        }
    }
}
