namespace RaceTrack.Models
{
    using RaceTrack.DAL;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;
    using System.ComponentModel;

    public class VehicleContext : DbContext
    {
        public VehicleContext() : base("VehicleContext")
        {
        }
        public virtual DbSet<Vehicle> Vehicles { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}