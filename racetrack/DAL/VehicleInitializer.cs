﻿using RaceTrack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaceTrack.DAL
{
    public class VehicleInitializer : System.Data.Entity.DropCreateDatabaseAlways<VehicleContext>
    {
        protected override void Seed(VehicleContext context)
        {
            var vehicles = new List<Vehicle>
            {
                new Vehicle {
                    Make = "Ford",
                    Model = "Edge SE",
                    Year = 2019,
                    VehType = "Car",
                    TowStrap = false,
                    TirePercentage = 90.0M,
                    ExtraClearance = 0,
                },
                new Vehicle {
                    Make = "Acura",
                    Model = "ILX",
                    Year = 2015,
                    VehType = "Car",
                    TowStrap = false,
                    TirePercentage = 50.0M,
                    ExtraClearance = 1,
                },
                new Vehicle {
                    Make = "Audi",
                    Model = "A5",
                    Year = 2019,
                    VehType = "Car",
                    TowStrap = false,
                    TirePercentage = 90.0M,
                    ExtraClearance = 1,
                },
                new Vehicle {
                    Make = "Chevrolet",
                    Model = "Silverado",
                    Year = 2017,
                    VehType = "Car",
                    TowStrap = true,
                    TirePercentage = 70.0M,
                    ExtraClearance = 2,
                },
                new Vehicle {
                    Make = "Toyota",
                    Model = "Camry",
                    Year = 2018,
                    VehType = "Car",
                    TowStrap = true,
                    TirePercentage = 85.0M,
                    ExtraClearance = 0,
                },
                new Vehicle {
                    Make = "Toyota",
                    Model = "4Runner",
                    Year = 2018,
                    VehType = "Truck",
                    TowStrap = true,
                    TirePercentage = 90.0M,
                    ExtraClearance = 6,
                },
                new Vehicle {
                    Make = "Nissan",
                    Model = "Frontier",
                    Year = 2015,
                    VehType = "Truck",
                    TowStrap = true,
                    TirePercentage = 75.0M,
                    ExtraClearance = 5,
                },
                new Vehicle {
                    Make = "GMC",
                    Model = "Canyon",
                    Year = 2020,
                    VehType = "Truck",
                    TowStrap = true,
                    TirePercentage = 100.0M,
                    ExtraClearance = 3,
                },
                new Vehicle {
                    Make = "Ford",
                    Model = "Ranger",
                    Year = 2016,
                    VehType = "Truck",
                    TowStrap = true,
                    TirePercentage = 80.0M,
                    ExtraClearance = 4,
                },
            };

            vehicles.ForEach(s => context.Vehicles.Add(s));
            context.SaveChanges();
        }
    }
}