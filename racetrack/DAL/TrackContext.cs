namespace RaceTrack.Models
{
    using RaceTrack.DAL;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;
    using System.ComponentModel;

    public class TrackContext : DbContext
    {
        public TrackContext() : base("TrackContext")
        {
        }

        public virtual DbSet<TrackVehicle> TrackVehicles { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}