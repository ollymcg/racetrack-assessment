﻿using RaceTrack.DAL;
using RaceTrack.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace RaceTrack
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Recreate test data on app start
            Database.SetInitializer(new VehicleInitializer());
            using (var db = new VehicleContext())
            {
                db.Database.Initialize(true);
            }
            Database.SetInitializer<TrackContext>(new DropCreateDatabaseAlways<TrackContext>());
            using (var db = new TrackContext())
            {
                db.Database.Initialize(true);
            }
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
