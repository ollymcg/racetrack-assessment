﻿using RaceTrack.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaceTrack.Models
{
    // Composition vs. Inheritance
    // Composition is useful when the class requires access to properties of multiple other interfaces without requiring full access.
    // Inheritance suggests the child class should share all the properties and creates a direct relationship to its parent.
    // In this case is it more practical to use inheritance because we only need to be able to access the attributes of the parent, no methods or properties are needed from another implementation
    public class TrackVehicle : Vehicle 
    {
        TrackService _trackService = new TrackService();

        public TrackVehicle() {
            // Logging multiple times because TrackContext is initialized every time view is returned
            _trackService.LogVehicles();
        }

    }
}