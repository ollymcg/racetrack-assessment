﻿using RaceTrack.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaceTrack.Models
{
    public class Vehicle
    {
        public Vehicle() {}

        public int ID { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string VehType { get; set; }
        public bool TowStrap { get; set; }
        public decimal TirePercentage { get; set; }
        public int ExtraClearance { get; set; }
    }
}